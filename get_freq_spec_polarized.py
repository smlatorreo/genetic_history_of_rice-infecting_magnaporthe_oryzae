# USAGE:
# Defolded: python3 get_freq_spec_polarized.py <file.vcf|.gz> <samples.txt> <outgroup1> ... <outgroup2>
# Folded: python3 get_freq_spec_polarized.py <file.vcf|.gz> -f <samples.txt>

import vcfpytools
from numpy import arange
from sys import argv

vcf = argv[1]
if '-f' in argv:
    folded = True
    samples_infile = argv[-1]
else:
    folded = False
    samples_infile = argv[2]
    ancestral = argv[3:]
    Nancestral = len(ancestral)

with open(samples_infile, 'r') as f:
    samples = [line.strip() for line in f]

if folded == True:
    allele_freqs = []
    for position in vcfpytools.get_genotypes(vcf, samples):
        if '.' not in position:
            geno = ((position.count('0')), (position.count('1')))
            allele_freqs.append(min(geno))
else:
    allele_freqs = []
    for position in vcfpytools.get_genotypes(vcf, samples + ancestral):
        if '.' not in position: # Full info sites
            ances_alleles = position[-Nancestral:]
            if ances_alleles.count('0') == Nancestral:
                allele_freqs.append(position[:-Nancestral].count('1'))
            elif ances_alleles.count('1') == Nancestral:
                allele_freqs.append(position[:-Nancestral].count('0'))

if folded == True:
    out = {i:allele_freqs.count(i) for i in range(1,int(len(samples)/2)+1)}
else:
    out = {i:allele_freqs.count(i) for i in range(1,len(samples))} #DAF 0 and 1 non-informative
total = sum(list(out.values()))

def ESFS(segsites, numsamples):
    '''This function estimates the Expected Site Frequency Spectrum (ESFS).
    ESFS(i) = theta / i ; where i is the given frequency (goes from 1 to n-1)
    theta can be replaced by its Watterson's estimator* (Watterson, G.A. 1975).
    * Assumes WF population at equilibrium and infinite sites mutation model)
    '''
    Eout = {i:0 for i in range(1,numsamples)}
    a = sum([1/i for i in range(1,numsamples)])
    for i in range(1,numsamples):
        Eout[i] = segsites / (i * a)
    return(Eout)

Eout = ESFS(total, len(samples))
if folded == True:
    Eout = {i:Eout[i] + Eout[len(samples)-i] for i in range(1, int(len(samples) * 0.5)+1)}

step = 0.1 # Change here the bin size
if folded == True:
    bins = arange(0,0.5,step)
else:
    bins = arange(0,1,step)
print('Bin_Start\tBin_End\tExp_Prop_Deriv_Sites\tObs_Prop_Deriv_Sites')
for bn in bins:
    binvals = []
    binEvals = []
    for count in out:
        if count / len(samples) > bn and count / len(samples) <= bn + step:
            binvals.append(out[count])
            binEvals.append(Eout[count])
    print(round(bn, 3), round(bn+step, 2), sum(binEvals) / sum(list(Eout.values())), sum(binvals) / sum(list(out.values())), sep = '\t')
