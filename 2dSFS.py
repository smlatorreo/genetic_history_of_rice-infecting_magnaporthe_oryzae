# USAGE:
# python3 2dSFS.py <file.vcf|.gz> <population1.txt> <population2.txt> <outgroup1> ... <outgroupn>
import vcfpytools
from numpy import arange, zeros, savetxt
from sys import argv, stdout

vcf = argv[1]

samples_infile1 = argv[2]
samples_infile2 = argv[3]
ancestral = argv[4:]

def twoDSFS(vcf, samples1, samples2, ancestral, missing_allow):
    breaks = (len(samples1), len(samples1) + len(samples2))
    Nancestral = len(ancestral)
    freqs_prop = {'pop1':[], 'pop2':[]}
    for position in vcfpytools.get_genotypes(vcf, samples1 + samples2 + ancestral):
        miss_pop1 = position[:breaks[0]].count('.')
        miss_pop2 = position[breaks[0]:breaks[1]].count('.')
        check1 = miss_pop1 / len(samples1) <= missing_allow
        check2 = miss_pop2 / len(samples2) <= missing_allow
        check3 = '.' not in position[-Nancestral:] and position[-Nancestral:][0] == position[-Nancestral:][1]
        if check1 == True and check2 == True and check3 == True:
            ancest_alleles = position[-Nancestral:]
            if ancest_alleles.count('0') == Nancestral:
                freqs_prop['pop1'].append(position[:breaks[0]].count('1') / (len(samples1)-miss_pop1))
                freqs_prop['pop2'].append(position[breaks[0]:breaks[1]].count('1') / (len(samples2)-miss_pop2))
            elif ancest_alleles.count('1') == Nancestral:
                freqs_prop['pop1'].append(position[:breaks[0]].count('0') / (len(samples1)-miss_pop1))
                freqs_prop['pop2'].append(position[breaks[0]:breaks[1]].count('0') / (len(samples2)-miss_pop2))
    return(freqs_prop)

with open(samples_infile1, 'r') as f:
    samples1 = [line.strip() for line in f]
with open(samples_infile2, 'r') as f:
    samples2 = [line.strip() for line in f]

tdSFS = twoDSFS(vcf, samples1, samples2, ancestral, 0.1) # Allow missingess of 10% in each population

for i in range(len(tdSFS['pop1'])):
    p1 = tdSFS['pop1'][i]
    p2 = tdSFS['pop2'][i]
    if 0 in (p1, p2) or 1 in (p1, p2):
        continue
    else:
        print(p1, p2, sep = '\t')
