# This set of commands will produce filtered vcf files for a given list of samples and its genetic distances

#### Change paths and variables accordingly ############
gatk=/path/to/gatk3.8.jar
plink=/path/to/plink1.9

reference=/path/to/reference/genome/Guy11PacBio.fasta
list=text_file_with_paths_for_genomic_vcfs_per_sample.txt
output=output_prefix
maxvmem=30G
###########################################################

# 1. Genotyping
java -Xmx$maxvmem -jar $gatk -T GenotypeGVCFs -R $reference -V $list -o $output.raw.vcf.gz

# 2. Selecting SNPs
java -Xmx$maxvmem -jar $gatk -T SelectVariants -R $reference -V $output.raw.vcf.gz -selectType SNP -o $output.raw.snps.vcf.gz &&

# 3. Filtering
java -Xmx$maxvmem -jar $gatk -T VariantFiltration -R $reference -V $output.raw.snps.vcf.gz --filterExpression "QD < 5.0" --filterName "QDFilter" --filterExpression "QUAL < 5000.0" --filterName "QualFilter" --filterExpression "MQ < 20.0" --filterName "MQ" --filterExpression "ReadPosRankSum < -2.0" --filterName "ReadPosRankSum" --filterExpression "ReadPosRankSum > 2.0" --filterName "ReadPosRankSum" --filterExpression "MQRankSum < -2.0" --filterName "MQRankSum" --filterExpression "MQRankSum > 2.0" --filterName "MQRankSum" --filterExpression "BaseQRankSum < -2.0" --filterName "BaseQRankSum" --filterExpression "BaseQRankSum > 2.0" --filterName "BaseQRankSum" -o $output.snps.filtered.vcf.gz

# 4. Population filter with full coverage and selection of biellelic snps
java -Xmx$maxvmem -jar $gatk -T SelectVariants -R $reference -V $output.snps.filtered.vcf.gz -o $output.snps.filtered.popfiltered100.biallelic.vcf.gz --maxNOCALLfraction 0 --restrictAllelesTo BIALLELIC

# 5. Calculation of Hamming distances
plink --allow-extra-chr --distance square --vcf-filter --vcf $output.snps.filtered.popfiltered100.biallelic.vcf.gz --out $output.snps.filtered.popfiltered100.biallelic
