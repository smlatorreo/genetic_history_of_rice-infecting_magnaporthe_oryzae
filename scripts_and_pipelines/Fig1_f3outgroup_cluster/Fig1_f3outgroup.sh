# Dataset

# Path to qp3pop from AdmixTools: https://github.com/DReichLab/AdmixTools
#qp3pop=/path/to/qp3pop
qp3pop=~/soft/AdmixTools/bin/qp3Pop

cd data

echo "Calculating f3 pairwise distances..."
$qp3pop -p config_file_qp3Pop.par > ../f3out.out
cd ..

grep "result:" f3out.out | awk '{print $2"\t"$3"\t"$5}' > f3out.f3s

R CMD BATCH data/Fig1_plot.R /dev/null
echo "Fig1 has been produced: Fig1.pdf"
