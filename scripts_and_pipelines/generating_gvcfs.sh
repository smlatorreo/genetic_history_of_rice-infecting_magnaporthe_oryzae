# This command will produce intermediate genomic vcf files per sample
# Change paths of the variables and set the maximum memory used by java

gatk=/path/to/gatk3.8.jar
reference=/path/to/reference/genome/Guy11PacBio.fasta
bam=/path/to/mapped/file.bam

maxmem=20G


java -Xmx$maxmem -jar $gatk -T HaplotypeCaller -R $reference -I $bam --genotyping_mode DISCOVERY -ERC GVCF -stand_call_conf 30 -o ${bam%.bam}.raw.g.vcf.gz -ploidy 1
